def modify_bit(number, position, value):
    mask = 1 << position
    return (number & ~mask) | ((value << position) & mask)