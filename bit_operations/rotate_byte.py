def rotateByte(num):
    sum = 0
    # go over the range of the byte (8 Bits)
    for i in range (7, -1, -1):
        print("index i: ",i, " shift :", 1 << 7-i, "value", num & (1 << 7-i))
        # check if bit on position i is set (see isKthBitSet)
        if num & (1 << i) > 0: 
            # add the inverted value of the bit to the sum e.g. 
            #if bit on leftmost pos. is set (128), rightmost value (1) is added 
            sum += 1 << 7-i  
    return sum

print (bin(rotateByte(0b10000000))) # 0b1
print (bin(rotateByte(0b00000001))) # 0b10000000
print (bin(rotateByte(0b00010000))) # 0b1000