def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val              

print(twos_comp(32,8))

def twosCompBack(number,bits):
    if number < 0:
        complement = (1<<bits-1) + (number & ((1<<bits-1)-1))
        print(1<<bits, number & ((1<<bits-1)-1), number, bin(number), complement, bin(complement), hex(complement))
        return complement
    else:
        return number
    
for i in range(-128, 127):   
    print(twosCompBack(i,8))
    
print(twosCompBack(-2147483648,32)) # print twosComplement
print(~(twosCompBack(-2147483648,32)) +1) # print real value



