def isKthBitSet(n, k): 
    if n & (1 << k): 
        return 1
    else: 
        return 0 
    
print(isKthBitSet(128,7)) # 1
print(isKthBitSet(128,6)) # 0