# Python_Function_Collection

A collection of different functions for several purposes

## TOC
- bit_operations
    - is_kth_bit_set
    - print_bin_data
    - rotate_byte
    - twos_comp
- data_manipulation
    - access_list_by_index
    - calc_attribute_probabilities
    - clean_objects
    - deep_copy
    - itertools_example
    - normalize_data
- displaying
    - plot_2d_cluster
    - print_data_in_table
- filesystems
    - csv_logger
    - json_data
    - lz4_compression
    - read_files
    - read_write_txt
- hardware
    - i2c_com
- misc
    - os_and_filesystem_functions
    - timestamps

## Setup

### Create Virtual Environment

```
python -m venv .venv
```

### Install Requirements from Text file

```
.\.venv\Scripts\pip.exe -m pip install -r requirements.txt
```


### Write Requirements top Text file

```
.\.venv\Scripts\pip.exe freeze > requirements.txt
```


