sudo apt update && sudo apt upgrade -y
sudo apt install python3 python3-pip -y
sudo pip3 install virtualenv

python3 -m venv $(pwd)/notebook_env
# source notebook_env/bin/activate

$(pwd)/notebook_env/bin/python3 -m pip install -r $(pwd)/setup/requirements_linux_notebook.txt 

$(pwd)/notebook_env/bin/jupyter notebook --port=16088