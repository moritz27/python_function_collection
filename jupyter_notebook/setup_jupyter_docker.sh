chmod -R 777 notebooks

NB_USER=kaeser

docker run \
    --detach=true \
    --restart="unless-stopped" \
    --name notebook \
    --publish 16088:8888 \
    --user root \
    -e NB_USER=${NB_USER} \
    -e CHOWN_HOME=yes \
    -w "/home/${NB_USER}" \
    --volume "${PWD}/notebooks":/home/${NB_USER}/ext_notebooks \
jupyter/minimal-notebook:latest 

## Get Token with: docker logs notebook

pip3 install numpy pandas influxdb_client matplotlib

#disabled token
docker run -it \
    --restart="unless-stopped" \
    --name notebook \
    --publish 16088:8888 \
    --user root \
    -e NB_USER="moritz" \
    -e CHOWN_HOME=yes \
    -w "/home/${NB_USER}" \
    --volume "${PWD}":/home/${NB_USER}/work \
    jupyter/minimal-notebook:latest \
    start-notebook.sh --NotebookApp.token=''


    # --detach=true \

docker run -it --rm \
    --publish 16088:8888 \
    --volume "${PWD}":/home/jovyan/work \
jupyter/datascience-notebook