import operator
from functools import reduce  # Python 3 forward compatibility 

def read_by_index(lst, indexes):
	return reduce(operator.getitem, indexes, lst) 
	
# Write
def write_by_index(lst, new_value, append = False, indexes=None):
    if indexes and len(indexes) > 0:
        target = read_by_index(lst, indexes[:-1])
        if append == True:
            target[indexes[-1]].append(new_value)
        else:
            target[indexes[-1]] = new_value
    else:
        lst = new_value
    return lst

data_list = [[1,2,3,4],[5,6,7,8]]

print(read_by_index(data_list, [0,3])) #should print 4

write_by_index(data_list, 9, indexes=[0,3])

print(data_list) # should print [[1, 2, 3, 9], [5, 6, 7, 8]]
