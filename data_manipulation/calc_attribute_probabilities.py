def determine_possible_options(data):
    """ returns all possible attribute of objects from given data set """
    possible_options = {}
    for datapoint in data:
        #print(data[datapoint])
        for option in data[datapoint]:
            #print(option, data[datapoint][option])
            if option not in possible_options:
                possible_options[option] = []
            if data[datapoint][option] not in possible_options[option]:
                possible_options[option].append(data[datapoint][option])
                
    #print(possible_options)
    return possible_options
    
def count_all_occurences(data, possible_options = None):
    """ returns how often attributes occure in the data """
    if possible_options == None:
        possible_options = determine_possible_options(data)
    #data_point_num = len(data)
    #print(data_point_num)
    occurences = {}
    for attribute in possible_options:
        occurences[attribute] = {}
        #print(attribute,":")
        for value in possible_options[attribute]:
            occurences[attribute][value] = count_occurrence(data, attribute, value)
            #print (value, count_occurrence(data, attribute, value))
    return(occurences)
    
def count_occurrence(data, attribute, value, class_condition = None):
    """ returns how often one specific attribute occurs """
    occurrence_counter = 0
    for datapoint in data:
        #print(data[datapoint], value, attribute, class_condition)
        if data[datapoint][attribute] == value:
            if class_condition == None :
                occurrence_counter += 1
            elif data[datapoint]["class"] == class_condition:
                occurrence_counter += 1
    return occurrence_counter

def calculate_probabilities(data, possible_options = None):
    """ returns the probabilities of all attributes in the data """
    if possible_options == None:
        possible_options = determine_possible_options(data)
    #print(possible_options)
    data_point_num = len(data)
    #print(data_point_num)
    option_probabilities = {}
    for attribute in possible_options:
        option_probabilities[attribute] = {}
        #print(attribute,":")
        for value in possible_options[attribute]:
            option_probabilities[attribute][value] = {}
            occurrence = count_occurrence(data, attribute, value)
            option_probabilities[attribute][value]["count"] = occurrence
            option_probabilities[attribute][value]["probability"] = occurrence / data_point_num
            #print (value, count_occurrence(data, attribute, value))
            if "class" in possible_options:
                if attribute != "class":
                    option_probabilities[attribute][value]["class"] = {}
                    sum = 0
                    for c in possible_options["class"]:
                        occurrence = count_occurrence(data, attribute, value, class_condition=c)
                        option_probabilities[attribute][value]["class"][c] = {"count": occurrence}
                        sum += occurrence
                    for c in possible_options["class"]:
                        option_probabilities[attribute][value]["class"][c]["probability"] = option_probabilities[attribute][value]["class"][c]["count"] /sum
                    #print(sum)
    return(option_probabilities)

data = {
"1": {
    "outlook":"sunny",
    "temperature":"hot",
    "humidity":"high",
    "wind":"weak",
    "class":"not_playing_tennis"
},
"2": {
    "outlook":"sunny",
    "temperature":"hot",
    "humidity":"high",
    "wind":"strong",
    "class":"not_playing_tennis"
},
"3": {
    "outlook":"overcast",
    "temperature":"hot",
    "humidity":"high",
    "wind":"weak",
    "class":"playing_tennis"
},
"4": {
    "outlook":"rain",
    "temperature":"mild",
    "humidity":"high",
    "wind":"weak",
    "class":"playing_tennis"
},
"5": {
    "outlook":"rain",
    "temperature":"cool",
    "humidity":"normal",
    "wind":"weak",
    "class":"playing_tennis"
},
"6": {
    "outlook":"rain",
    "temperature":"cool",
    "humidity":"normal",
    "wind":"strong",
    "class":"not_playing_tennis"
},
"7": {
    "outlook":"overcast",
    "temperature":"cool",
    "humidity":"normal",
    "wind":"strong",
    "class":"playing_tennis"
},
"8": {
    "outlook":"sunny",
    "temperature":"mild",
    "humidity":"high",
    "wind":"weak",
    "class":"not_playing_tennis"
},
"9": {
    "outlook":"sunny",
    "temperature":"cool",
    "humidity":"normal",
    "wind":"weak",
    "class":"playing_tennis"
},
"10": {
    "outlook":"rain",
    "temperature":"mild",
    "humidity":"normal",
    "wind":"weak",
    "class":"playing_tennis"
},
"11": {
    "outlook":"sunny",
    "temperature":"mild",
    "humidity":"normal",
    "wind":"strong",
    "class":"playing_tennis"
},
"12": {
    "outlook":"overcast",
    "temperature":"mild",
    "humidity":"high",
    "wind":"strong",
    "class":"playing_tennis"
},
"13": {
    "outlook":"overcast",
    "temperature":"hot",
    "humidity":"normal",
    "wind":"weak",
    "class":"playing_tennis"
},
"14": {
    "outlook":"rain",
    "temperature":"mild",
    "humidity":"high",
    "wind":"strong",
    "class":"not_playing_tennis"
}
}

print(calculate_probabilities(data))