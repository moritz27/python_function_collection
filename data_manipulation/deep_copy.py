import copy
source_data = {"A":{"B":1,"C":2}}
ref_copy = source_data
deep_copy = copy.deepcopy(source_data)

print(id(source_data) == id(ref_copy)) #should print true
print(id(source_data) == id(deep_copy)) #should print false