def filter_data(data, filters):
    filtered_data = {}
    for point in data:
        add_point = 1
        for i in range (0, len(filters[0])):
            #print(point, filters[0][i], filters[1][i], data[point])
            if filters[0][i] in data[point]:
                if data[point][filters[0][i]] != (filters[1][i]):
                    add_point = 0
                    #break
        if add_point == 1:
            filtered_data[point] = data[point]
    return filtered_data

def remove_attribute(data, attribute):
    cleaned_data = data.deepcopy()
    for point in data:
        print(attribute, cleaned_data[point])
        try:
            del cleaned_data[point][attribute]
        except:
            print("error")
    return cleaned_data


list_to_keep = ["name","succsess_time","succsess"]
def keep_attributes(data, list_to_keep):
    new_dict = { key: data[key] for key in list_to_keep }