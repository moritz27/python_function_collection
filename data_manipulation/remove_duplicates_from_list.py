def __remove_duplicates_from_list(self, input_list):
    return list(set(input_list))