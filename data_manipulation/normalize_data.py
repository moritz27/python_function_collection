import random, math

def calc_normalization_coefficients(data):
    """ returns parameters for the nomralization """   
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    datatypes = {} 
    extrema = {}
    extrema["max_values"] = [None for i in range(dimensions)]
    extrema["min_values"] = [None for i in range(dimensions)]
    extrema["power"] = [None for i in range(dimensions)]
    
    for point in data:
        for dimension in range(0, dimensions):
            if extrema["max_values"][dimension] == None or data[point][dimension] > extrema["max_values"][dimension]:
                extrema["max_values"][dimension] = data[point][dimension]
            if extrema["min_values"][dimension] == None or data[point][dimension] < extrema["min_values"][dimension]:
                extrema["min_values"][dimension] = data[point][dimension]
            if abs(extrema["min_values"][dimension]) > abs(extrema["max_values"][dimension]):
                extrema["power"][dimension] = 10 ** math.log10(abs(extrema["min_values"][dimension]))
            elif abs(extrema["max_values"][dimension]) > abs(extrema["min_values"][dimension]):
                extrema["power"][dimension] = 10 ** math.floor(math.log10(abs(extrema["max_values"][dimension])))
            datatypes[dimension] = type(data[point][dimension])
    
    #print(extrema)

    offsets = [0 for i in range(dimensions)]
    spans = [0 for i in range(dimensions)]
    
    for dimension in range(0, dimensions):
        if extrema["max_values"][dimension] < 0:
            offsets[dimension] = abs(extrema["min_values"][dimension])
            spans[dimension] = abs(extrema["min_values"][dimension]) - abs(extrema["max_values"][dimension])
        else:
            if extrema["min_values"][dimension] < 0:
                spans[dimension] = abs(extrema["min_values"][dimension]) + extrema["max_values"][dimension]
                offsets[dimension] = abs(extrema["min_values"][dimension])
            else:
                spans[dimension] = abs(extrema["max_values"][dimension])
        
    #print(offsets,spans)

    coefficients = {
        "offsets": offsets,
        "spans": spans,
        "extrema": extrema,
        "datatypes": datatypes
    } 
    
    return coefficients

def normalize_point(point, data = None, normalize_coefficients = None, backwards = False):
    """ returns one point converted to/from normalized range """ 
    if normalize_coefficients == None:
        normalize_coefficients = calc_normalization_coefficients(data)
    dimensions = len(point)
    norm_point = []
    for dimension in range(0, dimensions):
            #print(data[point][dimension], "+", offsets[dimension],"=", (data[point][dimension] + offsets[dimension]), "/", spans[dimension], "=", (data[point][dimension] + offsets[dimension]) / spans[dimension] )
            if backwards == False:
                norm_point.append((point[dimension] + normalize_coefficients["offsets"][dimension]) / normalize_coefficients["spans"][dimension])
            elif backwards == True:
                norm_point.append((normalize_coefficients["datatypes"][dimension])(round_up(((point[dimension] - normalize_coefficients["offsets"][dimension]) * normalize_coefficients["spans"][dimension]), normalize_coefficients["extrema"]["power"][dimension]/100)))
    return norm_point

def round_up(num, factor):
    """ returns a number rounded up to the given factor """
    return int(math.ceil(num / factor)*factor)

def round_down(num, factor):
    """ returns a number rounded down to the given factor """
    return int(math.floor(num / factor)*factor)


def normalize_data(data, normalize_coefficients = None, backwards = False):
    """ returns the data (dict or nested list) converted to/from normalized range """ 
    if normalize_coefficients == None:
        normalize_coefficients = calc_normalization_coefficients(data)
    norm_data = None
    for point in data:
        if type(point) == list:
            if norm_data == None:
                norm_data = []
            norm_data.append(normalize_point(point, normalize_coefficients = normalize_coefficients, backwards = backwards))
        else:
            if norm_data == None:
                norm_data = {}
            norm_data[point] = normalize_point(data[point], normalize_coefficients = normalize_coefficients, backwards = backwards)
    result = {
        "data": norm_data,
        "coefficients": normalize_coefficients
    }
    return result


data = {
    "DP1": [22,2200],
    "DP2": [22,2400],
    "DP3": [23,2300],
    "DP4": [24,2450],
    "DP5": [26,2200],
    "DP6": [28,4900],
    "DP7": [29,5100],
    "DP8": [29,5200],
    "DP9": [31,4800],
    "DP10": [32,5200],
    "DP11": [34,5000],
    "DP12": [41,3800],
    "DP13": [42,3850],
    "DP14": [42,3100],
    "DP15": [43,3700],
    "DP16": [44,3600],
    "DP17": [45,3750],
    "DP18": [47,3600],
    "DP19": [51,4200],
    "DP20": [55,3800],
    "DP21": [56,3600], 
    "DP22": [57,3650], 
    "DP23": [57,3950],
    "DP24": [58,3550],
    "DP25": [66,2800]   
}

#normalize_coefficients = calc_normalize_coefficients(data)
#print(normalize_coefficients)

# normalize all points
normalized = normalize_data(data)
print(normalized)

# norm_point = normalize_point([66,2800], data = data)
# print(norm_point)

# unnormalize a single point
#unnormalized_point = normalize_point([0.8787878787878788, 0.6826923076923077], normalize_coefficients, backwards = True)
#print("unnormalized_point", unnormalized_point) # should print [58, 3550]

# unnormalize all points
#unnormalized_data = normalize_data(normalized_data, normalize_coefficients, backwards = True)
#print(unnormalized_data)

#check if the back converted output is the same as the input
#print(data == unnormalized_data)