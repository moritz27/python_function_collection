# https://docs.python.org/3/library/itertools.html

import itertools as it

data = [[1,2,3],[4,5,6]]

#cartesian product 
print(list(it.product(*data)))

#permutation (r-length tuples, all possible orderings, no repeated elements)
print(list(it.permutations(data[0])))

#combinations (r-length tuples, in sorted order, no repeated elements)
print(list(it.combinations(data[0],2)))

#combinations_with_replacement (r-length tuples, in sorted order, with repeated elements)
print(list(it.combinations_with_replacement(data[0],2)))
