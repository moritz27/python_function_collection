import math

def float_32bit_to_bytes(num):
    fractional, whole = math.modf(num)
    byte_array = bytearray(4)
    byte_array[0] = int(whole) >> 8
    byte_array[1] = int(whole) & 0xFF 
    byte_array[2] = int(fractional * 1000) >> 8 
    byte_array[3] = int(fractional * 1000) & 0xFF
    return byte_array

byte_array = float_32bit_to_bytes(1234.567)

#print((byte_array[0]<<8), byte_array[1], (byte_array[2]), (byte_array[3]))
#print((byte_array[0]<<8) + byte_array[1], (byte_array[2] <<8)/1000 + (byte_array[3])/1000)
print((byte_array[0]<<8) + byte_array[1] + (byte_array[2] <<8)/1000 + byte_array[3]/1000)