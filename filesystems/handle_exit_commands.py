# From: https://chadrick-kwag.net/python-interrupt-sigterm-sigkill-exception-handling-experiments/
 
import signal, sys
 
def int_handler(signum, frame):
    print("sigint handler")
    sys.exit(0)
signal.signal(signal.SIGINT, int_handler)
 
def term_handler(signum, frame):
    print("sigterm handler")
    sys.exit(0)
signal.signal(signal.SIGTERM, term_handler)
 
try:
    print("entering loop")
    while True:
        pass
except KeyboardInterrupt:
    print("keyboardinterrupt detected")
except Exception as e:
    print("exception catched")