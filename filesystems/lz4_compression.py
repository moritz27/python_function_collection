import lz4.frame
import json

data = {
"Haus":1,
"Maus":2,
"Laus":3
}

def write_lz4_json(path, data):
	write_data =  json.dumps(data, indent=4).encode()
	#compressed = lz4.frame.compress(write_data)
	with lz4.frame.open(path, mode='wb') as fp:
		fp.write(write_data)

write_lz4_json("testfile.lz4", data)

 
def read_lz4_json(path):
	with lz4.frame.open(path, mode='r') as fp:
		read_data = json.loads(fp.read())
	return read_data

print(json.dumps(read_lz4_json("testfile.lz4"), indent=2))