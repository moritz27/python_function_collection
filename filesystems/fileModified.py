import hashlib
import time


def sha1(filename):
    BUF_SIZE = 65536  # read stuff in 64kb chunks!
    sha1 = hashlib.sha1()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()


def watchFile(filename):
    lastHash = sha1(filename)
    while True:
        newHash = sha1(filename)
        if newHash != lastHash:
            # print(newHash, lastHash)
            lastHash = newHash
            print(filename, "has changed!")
        time.sleep(1)


def compareFile(filename, lastHash):
    newHash = sha1(filename)
    if newHash != lastHash:
        print(filename, "has changed!")
    return newHash


lastHash = sha1("test.txt")
while True:
    lastHash = compareFile("test.txt", lastHash)

# watchFile("test.txt")
