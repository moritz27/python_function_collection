import os

def move_existing_file_to_folder(self, filename):
    """Moves an existing gz file to the destination folder."""
    src_path = os.path.join(self.storage_dir, filename)
    if os.path.exists(src_path):
        dest_folder = self.create_storage_folder(filename)
        dest_path = os.path.join(dest_folder, filename)
        os.rename(src_path, dest_path)

def move_all_files(self, metadata):
    """Move all existing files to new storage location"""
    for i in range(0, metadata["datafile_count"]):
        self.move_existing_file_to_folder(metadata["filenames"][i])