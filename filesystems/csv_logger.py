from datetime import datetime

class DATA_LOGGER_CLASS:
    def __init__(self, header, path = "", name = "logfile"):
        self.file = self.create(header, path, name)

    def get_datetime(self):
        # datetime object containing current date and time
        now = datetime.now()
        #print("now =", now)
        # dd/mm/YY H:M:S
        dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")
        #print("date and time =", dt_string)
        return dt_string

    def create(self, header, path, name):
        file_name_string = path + name + "_" + self.get_datetime() + ".csv"
        file = open(file_name_string, "a")
        for colum in header:
            file.write(colum)
            file.write(',')
        file.write('\n')
        return file

    def write(self, linedata):
        for i in range (0, len(linedata)):
            self.file.write(str(linedata[i]))
            if i < (len(linedata)-1):
                self.file.write(',')
        self.file.write('\n')

    def close(self):
        self.file.close()


    def convert_gps_data(self, data, precision = 6):
        try:
            coordinates = {
                "longitude":9999.999999,
                "latitude":9999.999999
            }
                
            #convert NMEA format to coordinates
            coordinates["longitude"] = int(data[0:2]) + round(((float(data[2:(5+precision)])  / 60.0)), precision)
            coordinates["latitude"] = int(data[(6+precision):(8+precision)]) + round(((float(data[(8+precision):(16+precision)]) / 60.0)), precision)

            print(coordinates["longitude"],",", coordinates["latitude"])
            
            return (coordinates)
        except:
            return



from random import seed
from random import random
# seed random number generator
seed(1)


header = ("longitude", "latitude", "signalstrength")
path = "logs/"
logger = DATA_LOGGER_CLASS(header)

try:
    for i in range (0,100):
        linedata = (random(), random(), random())
        logger.write(linedata)

finally:
    logger.close()