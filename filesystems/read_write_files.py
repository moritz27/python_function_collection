def read_json_file(self, filepath):
    """Reads the metadata from JSON file"""
    if os.path.exists(filepath):
        print("%s found!" %filepath)
        try:
            if filepath.endswith(".gz"):
                with gzip.open(filepath, 'r') as f:
                    file_content = json.loads(f.read())
            else:
                with open(filepath, 'r') as f:
                    file_content = json.loads(f.read())
        except:
            print("Error occured parsing %s" %filepath)
            file_content = None
        finally:
            return file_content
    else:
        print("%s not found!" %filepath)
        return None

def save_json_file(self, data, filename, path=None):
    """Creates or updates the file to store metadata."""
    if path == None:
        path = self.script_dir
    if os.path.exists(path):
        path = os.path.join(path, filename)
        # print("Saving file to", path)
        try:
            with open(path, 'w') as f:
                f.write(json.dumps(data, indent=4, sort_keys=True))
        except Exception as e:
            print("Error writing file: ", e)

def read_csv_file(self, filepath):
    """Reads the metadata from CSV file or creates new."""
    if os.path.exists(filepath):
        print("%s found!" %filepath)
        try:
            if filepath.endswith(".gz"):
                with gzip.open(filepath, "rt", newline="") as file:
                    file_content =  list(csv.reader(file, delimiter=","))
            else:
                with open(filepath, "rt", newline="") as file:
                    file_content =  list(csv.reader(file, delimiter=","))
        except:
            print("Error occured parsing %s" %filepath)
            file_content = None
        finally:
            return file_content
    else:
        print("%s not found!" %filepath)
        return None

def load_data_from_gz(self, filepath):
    """Load and return data from gz file."""
    if os.path.exists(filepath):
        if filepath[:-3].endswith(".csv") or filepath[:-3].endswith(".log"):
            return self.read_csv_file(filepath)
        elif filepath[:-3].endswith(".json"):
            return self.read_json_file(filepath)
    else:
        print(filepath, "not found!")