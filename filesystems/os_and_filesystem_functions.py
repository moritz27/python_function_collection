#create venv
#install: pip3 install python3-venv
#activate: source venv/bin/activate
#deactivate: deactivate

#pip3 install pytest-shutil
import os, shutil

path = "F:\Programs"

#get current working directory
os.getcwd()

#join paths
os.path.join(path, filename)

#check if path/file exists 
print(os.path.exists(""))

#create folder
if(os.path.exists("test")):
    os.rmdir("test")
os.mkdir("test")
os.rmdir("test")

#create and remove folders (recursive)
os.makedirs("test/test")
if(os.path.exists("test/test")):
    shutil.rmtree("test/test")
    
#check file ending
filename.endswith(".txt")

#import from parent folder 
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

#shut the system down from python
import os
os.system("/sbin/shutdown -h now")

#run command from python
import os
command = os.popen("bash 'get_google_time.sh'")
print(command.read())
print(command.close())


# list all files 
from os import listdir
from os.path import isfile, join
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]


# list all folders in dir
import os 
my_list = os.listdir(path)