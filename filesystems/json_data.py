import os, json
path = str(os.path.dirname(os.path.abspath(__file__)))+"/data.json"
# create a dictionary
writeData={}
#fill the dictionary with content
for i in range (0,10):
    writeData[i]=[]
    for j in range (0,10):
        writeData[i].append(j)
#write the data to JSON file
file= open(path,"w+")
json_string = json.dumps(writeData, indent=2)
file.write(json_string)
file.close()
#read the data back
file = open(path,"r")
readData = json.loads(file.read())
file.close()
#print the data
print(readData)