import re, json, gzip, csv, os

def read_csv_from_file(path, split_char = "\s+"):
    """ returns object from CSV file """
    data = {}
    f = open(path, 'r')
    Lines = f.readlines()
    count = 0
    # Strips the newline character
    for line in Lines:
        count += 1
        name = "DP"+str(count)
        data[name] = []
        point = re.split(split_char, line.strip())
        #print(point)
        for dim in range(0, len(point)):
            data[name].append(int(point[dim]))
        #print("Line{}: {}".format(count, line.strip()))
    return data

def read_json_from_file(path):
    """ returns object from JSON file """
    data = {}
    f = open(path, 'r')
    data = json.load(f)
    return data

def read_json_file(self, filepath):
    """Reads the data from JSON file"""
    if os.path.exists(filepath):
        print("%s found!" %filepath)
        try:
            if filepath.endswith(".gz"):
                with gzip.open(filepath, 'r') as f:
                    file_content = json.loads(f.read())
            else:
                with open(filepath, 'r') as f:
                    file_content = json.loads(f.read())
        except:
            print("Error occured parsing %s" %filepath)
            file_content = None
        finally:
            return file_content
    else:
        print("%s not found!" %filepath)
        return None

def save_json_file(self, data, filename, path=None):
    """Creates or updates the file to store data."""
    if path == None:
        path = self.script_dir
    if os.path.exists(path):
        path = os.path.join(path, filename)
        # print("Saving file to", path)
        try:
            with open(path, 'w') as f:
                f.write(json.dumps(data, indent=4, sort_keys=True))
        except Exception as e:
            print("Error writing file: ", e)
    
def read_csv_file(self, filepath):
    """Reads the content from CSV file or creates new."""
    if os.path.exists(filepath):
        print("%s found!" %filepath)
        try:
            if filepath.endswith(".gz"):
                with gzip.open(filepath, "rt", newline="") as file:
                    file_content =  list(csv.reader(file, delimiter=","))
            else:
                with open(filepath, "rt", newline="") as file:
                    file_content =  list(csv.reader(file, delimiter=","))
        except:
            print("Error occured parsing %s" %filepath)
            file_content = None
        finally:
            return file_content
    else:
        print("%s not found!" %filepath)
        return None

def load_data_from_gz(self, filepath):
    """Load and return data from gz file."""
    if os.path.exists(filepath):
        if filepath[:-3].endswith(".csv") or filepath[:-3].endswith(".log"):
            return self.read_csv_file(filepath)
        elif filepath[:-3].endswith(".json"):
            return self.read_json_file(filepath)
    else:
        print(filepath, "not found!")

def parse_can_log_from_csv(self, csv_content):
    """Parse the data from CSV lines to list of dicts."""
    data = []
    for item in csv_content:
        try:
            parsed_item = {}
            parsed_item["timestamp"] = int(item[0])
            parsed_item["CAN_ID"] = int(item[1], 16)
            parsed_item["CAN_DATA"] = int(item[2], 16).to_bytes(8, 'big')
            data.append(parsed_item)
        except Exception as e:
            print(item, "raised parsing error:", e)
    return data