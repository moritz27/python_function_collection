# list all folders in dir and write to file
import os, sys, json, datetime
from sys import platform

backup_folder_list = [
    "C:\Program Files",
    "C:\Program Files (x86)",
    "F:\EigeneDateien\repos",
    "F:\Programs"
]

file_extensions = [
    ".exe",
    ".msi",
    ".zip",
    ".rar",
    ".7z"
]

script_dir = os.path.dirname(os.path.realpath(__file__))
output_dir = os.path.join("F:", os.sep,"EigeneDateien","owncloud","Tech","folder_names")
print(output_dir)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("using script location...")
        if platform == "linux" or platform == "linux2":
            print("Linux")
            path = os.path.dirname(os.path.abspath(sys.argv[0]))
        elif platform == "darwin":
            print("MAC OS")
        elif platform == "win32":
            print("Windows")
            path = os.path.dirname(os.path.abspath(sys.argv[0]))
    elif len(sys.argv) == 2:
        path = os.path.abspath(sys.argv[1])
        output_dir = os.path.join("F:", os.sep,"EigeneDateien","owncloud","Tech","folder_names")
    elif len(sys.argv) == 3:
        path = os.path.abspath(sys.argv[1])
        output_dir = os.path.abspath(sys.argv[2])
    

    items = os.listdir(path)
    dir_names = []
    for i in items:
        #print(i,os.path.isdir(os.path.join(path, i)))
        for file_extension in file_extensions:
            if os.path.join(path, i).endswith(file_extension) or os.path.isdir(os.path.join(path, i)):
                dir_names.append(i)

    if len(dir_names) < 1:
        print("no folders in this directory")
    else:
        pass
        #print(dir_names)


    name = str(path).rsplit(os.sep)[-1]# + "_info"
    #print(name)

    filepath = os.path.join(output_dir, name + ".json")

    save_data = {}
    save_data["path"] = path
    save_data["datetime"] = str(datetime.datetime.now())
    save_data["dir_names"] = dir_names

    file= open(filepath,"w")
    json_string = json.dumps(save_data, indent=2)
    file.write(json_string)
    file.close()

