# import subprocess
  
# pytonProcess = subprocess.check_output("ps -ef | grep nrf24_raspi_gateway.py",shell=True).decode()
# pytonProcess = pytonProcess.split('\n')
  
# for process in pytonProcess:
#     p = process.split(" ")
#     print(p)


import psutil
# def checkIfProcessRunning(processName):
#     '''
#     Check if there is any running process that contains the given name processName.
#     '''
#     #Iterate over the all the running process
#     for proc in psutil.process_iter():
#         # print(proc)
#         try:
#             # Check if process name contains the given name string.
#             if processName.lower() in proc.name().lower():
#                 return proc.cmdline()

#         except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
#             pass
#     return False

# print(checkIfProcessRunning("python"))

for proc in psutil.process_iter():
    # print(proc.cmdline())
    command = " ".join(proc.cmdline())
    if "nrf24_raspi_gateway.py" in command:
        print(command, proc.pid)
        proc.terminate()