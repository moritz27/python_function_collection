import psutil


def checkIfProcessRunning(processName):
    ''' Check if there is any running process that contains the given name processName.'''
    # Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # print(proc.name())
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


if not (checkIfProcessRunning("pigpiod")):
    pass
else:
    print("pigpiod is running")
