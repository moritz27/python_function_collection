import serial

serialPort = 'COM39'
serialBaud = 2000000
dataNumBytes = 2


class ReadLine:
    def __init__(self, s):
        self.buf = bytearray()
        self.s = s

    def readline(self):
        i = self.buf.find(b"\n")
        if i >= 0:
            r = self.buf[:i+1]
            self.buf = self.buf[i+1:]
            return r
        while True:
            i = max(1, min(2048, self.s.in_waiting))
            data = self.s.read(i)
            i = data.find(b"\n")
            if i >= 0:
                r = self.buf + data[:i+1]
                self.buf[0:] = data[i+1:]
                return r
            else:
                self.buf.extend(data)



serialConnection = serial.Serial(serialPort, serialBaud, timeout=None)

rl = ReadLine(serialConnection)

while True:
    print(rl.readline())