import matplotlib.pyplot as plt
import numpy as np
import random, statistics
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
from scipy.spatial import ConvexHull


def Plot_Clusters_2d(data, clusters, plt_name = None, centroids = None, add_point = None, show_names = False):
    """ displays a 2-D Cluster Plot """
    plt.figure("2-D Cluster Plot")
    if plt_name != None:
        plt.title(plt_name)
    rand_key = random.choice(list(data.keys()))
    dimensions = len(data[rand_key])
    plot_data = {}
    plot_point_names = []
    #print(clusters)
    for c in clusters:
        plot_data[c] = [[] for i in range(dimensions)]
        #print(plot_data)
        for p in clusters[c]:
            #print(p, data[p])
            plot_point_names.append(p)
            for s in range(0, dimensions):
                #print(s, data[p][s])
                plot_data[c][s].append(data[p][s])
    #print(plot_data, plot_point_names)
    counter = 0
    cluster_counter = 0
    x_min = None
    x_max = None
    y_min = None
    y_max = None

    for pd in plot_data:
        r = random.random()
        b = random.random()
        g = random.random()
        c = (r, g, b)
        if show_names == True:
            plt.scatter(plot_data[pd][0],plot_data[pd][1], s=150, facecolors='none', edgecolors=c ,label=pd) 
        else:
            plt.scatter(plot_data[pd][0],plot_data[pd][1], marker="D", color=c ,label=pd) 
        if centroids != None:
            plt.scatter(centroids[cluster_counter][0],centroids[cluster_counter][1], s=50, color =c) 
        # plt.annotate(pd, # this is the text
        #                 (centroids[cluster_counter][0],centroids[cluster_counter][1]), # this is the point to label
        #                 textcoords="offset points", # how to position the text
        #                 xytext=(0,-3.5), # distance from text to points (x,y)
        #                 ha='center') # horizontal alignment can be left, right or center

        x = np.array(plot_data[pd][0]) 
        y = np.array(plot_data[pd][1])
        xy = np.hstack((x[:,np.newaxis],y[:,np.newaxis]))
        try:
            hull = ConvexHull(xy)
            for simplex in hull.simplices:
                plt.plot(x[simplex], y[simplex], color=c)
        except:
            pass
            #print("not enough points for hull")
        
        
        for p in range (0, len(plot_data[pd][0])):
            if show_names == True:
                plt.annotate(plot_point_names[counter], # this is the text
                            (plot_data[pd][0][p],plot_data[pd][1][p]), # this is the point to label
                            textcoords="offset points", # how to position the text
                            xytext=(0,-3.5), # distance from text to points (x,y)
                            ha='center') # horizontal alignment can be left, right or center
            if x_max == None or plot_data[pd][0][p] > x_max:
                x_max = plot_data[pd][0][p]
            if x_min == None or plot_data[pd][0][p] < x_min:
                x_min = plot_data[pd][0][p]
            if y_max == None or plot_data[pd][1][p] > y_max:
                y_max = plot_data[pd][1][p]
            if y_min == None or plot_data[pd][1][p] < y_min:
                y_min = plot_data[pd][1][p]
            counter+=1
        cluster_counter+=1

    #x_max += 1
    #y_max += 1

    # for centroid in centroids:
    #     print(centroid)
    #     plt.scatter(centroid[0],centroid[1], s=150, color = "b") 

    #print(x_min, x_max, y_min, y_max)

    if add_point != None:
        plt.scatter(add_point[0],add_point[1], s=50, c = "k", marker= "x") 

    plt.legend()
    plt.xticks(np.linspace(x_min, x_max, 11, dtype=int, endpoint = True)) 
    plt.yticks(np.linspace(y_min, y_max, 11, dtype=int, endpoint = True)) 
    plt.grid()
    plt.show()

data = {
    "A": [3,2],
    "B": [2,5],
    "C": [2,7],
    "D": [1,8],
    "E": [2,9],
    "F": [2,8],
    "G": [3,9],
    "H": [7,9],
    "I": [6,2],
    "J": [7,1],
    "K": [7,3],
    "L": [7,2],
    "M": [8,3],
    "N": [9,2],
    "P": [8,2],
    "R": [8,1],
    "S": [10,10],
    "T": [10,11],
    "U": [11,10],
    "V": [11,11]  
}

Plot_Clusters_2d(data)