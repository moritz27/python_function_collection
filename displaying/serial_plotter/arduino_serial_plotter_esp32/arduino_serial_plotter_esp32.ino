unsigned long timer = 0;
long loopTime = 10000;   // microseconds
 
void setup() {
  Serial.begin(2000000);
  //Serial.println(sizeof(double));
  timer = micros();
}
 
void loop() {
  timeSync(loopTime);
  int16_t value = analogRead(13);
  //double value = (analogRead(13));
  //Serial.print(value);
  //Serial.println("here");
  sendToPC(&value);
}
 
void timeSync(unsigned long deltaT)
{
  unsigned long currTime = micros();
  long timeToDelay = deltaT - (currTime - timer);

  //Serial.print(currTime);
  //Serial.print(" ");
  //Serial.println(timeToDelay);
  
  if (timeToDelay > 5000)
  {
    delay(timeToDelay / 1000);
    delayMicroseconds(timeToDelay % 1000);
  }
  else if (timeToDelay > 0)
  {
    delayMicroseconds(timeToDelay);
  }
  else
  {
      // timeToDelay is negative so we start immediately
  }
  timer = currTime + timeToDelay;
}
 
void sendToPC(int16_t* data)
{
  byte* byteData = (byte*)(data);
  Serial.write(byteData, 2);
}
 
void sendToPC(double* data)
{
  byte* byteData = (byte*)(data);
  Serial.write(byteData, 8);
  //Serial.write(13); // CR
  //Serial.write(10); // LF
}
