# import numpy as np
# import matplotlib.pyplot as plt
# from scipy.spatial import ConvexHull

# points = np.random.rand(30, 2)   # 30 random points in 2-D
# print(points)
# hull = ConvexHull(points)

# #xs = np.array([point[0] for point in points]) 
# #ys = np.array([point[1] for point in points]) 

# #xh = np.array([point[0] for point in hull.points]) 
# #yh = np.array([point[1] for point in hull.points]) 

# plt.plot(points[:,0], points[:,1], 'o')
# for simplex in hull.simplices:
#     plt.plot(points[simplex, 0], points[simplex, 1], 'k-')


# plt.plot(points[hull.vertices,0], points[hull.vertices,1], 'r--', lw=2)
# plt.plot(points[hull.vertices[0],0], points[hull.vertices[0],1], 'ro')
# plt.show()

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import ConvexHull

# RANDOM DATA
x = np.random.normal(0,1,10)
y = np.random.normal(0,1,10)
xy = np.hstack((x[:,np.newaxis],y[:,np.newaxis]))
print("x", x,"y", y,"xy", xy)

# PERFORM CONVEX HULL
hull = ConvexHull(xy)

# PLOT THE RESULTS
plt.scatter(x,y)
plt.plot(x[hull.vertices], y[hull.vertices])
plt.show()