#pip3 install beautifultable
from beautifultable import BeautifulTable

def determine_possible_options(data):
    """ returns all possible attribute of objects from given data set """
    possible_options = {}
    for datapoint in data:
        #print(data[datapoint])
        for option in data[datapoint]:
            #print(option, data[datapoint][option])
            if option not in possible_options:
                possible_options[option] = []
            if data[datapoint][option] not in possible_options[option]:
                possible_options[option].append(data[datapoint][option])
                
    #print(possible_options)
    return possible_options

def print_data_in_table(data , possible_options = None):
    """ prints the data in a table to the console """
    #print("Data:")
    table = BeautifulTable()
    if possible_options == None:
        possible_options = determine_possible_options(data)
    #print(possible_options)

    header = ["Datapoint"]
    for attribute in possible_options:
        header.append(attribute)
    table.rows.append(header)

    for dp in data:
        line_data = [dp]
        for attribute in data[dp]:
            line_data.append(data[dp][attribute])
        table.rows.append(line_data)
    print(table)
	
mushroom_data = {
    "DP1" : {
        "cap-shape":"CONVEX",
        "bruises":"BRUISES",
        "odor":"ALMOND"
    },
    "DP2" : {
        "cap-shape":"CONVEX",
        "bruises":"BRUISES",
        "odor":"ANISE"
    },
    "DP3" : {
        "cap-shape":"BELL",
        "bruises":"BRUISES",
        "odor":"ALMOND"
    }, 
    "DP4" : {
        "cap-shape":"CONVEX",
        "bruises":"NO",
        "odor":"NONE"
    }, 
    "DP5" : {
        "cap-shape":"FLAT",
        "bruises":"BRUISES",
        "odor":"PUNGENT"
    }, 
    "DP6" : {
        "cap-shape":"CONVEX",
        "bruises":"NO",
        "odor":"NONE"
    }, 
    "DP7" : {
        "cap-shape":"CONVEX",
        "bruises":"BRUISES",
        "odor":"NONE"
    }, 
    "DP8" : {
        "cap-shape":"CONVEX",
        "bruises":"BRUISES",
        "odor":"NONE"
    }, 
    "DP9" : {
        "cap-shape":"FLAT",
        "bruises":"BRUISES",
        "odor":"ANISE"
    }, 
    "DP10" : {
        "cap-shape":"CONVEX",
        "bruises":"NO",
        "odor":"PUNGENT"
    }  
}

print_data_in_table(mushroom_data)