import numpy as np
import matplotlib.pyplot as plt
import random

data_theta = range(10,171,10)

data_theta_rad = []
for i in data_theta:
    data_theta_rad.append(float(i)*np.pi/180.0)

data_r = random.sample(range(70, 90), 17)

print (data_theta)
print (data_r)

ax = plt.subplot(111, polar=True)
ax.set_theta_zero_location("N")
ax.set_theta_direction(-1)
ax.plot(data_theta_rad, data_r, color='r', linewidth=3)
ax.set_rmax(95)
# ax.set_rmin(70.0)
ax.grid(True)

ax.set_title("Example", va='bottom')
plt.show()