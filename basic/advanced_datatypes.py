# create array with n * value
array = [0]*10
print(array)

# reverse a list
a = [1,2,3,4,5]
b = a[::-1] #[5,4,3,2,1]
print(b)

# List Comprehension
# result = [f(x) for list[x] if condition]
squares = [x**2 for x in range(10) if (x % 2 == 0)]
print(squares)

# cross product
colours = [ "red", "green", "yellow", "blue" ]
things = [ "house", "car", "tree" ]
coloured_things = [ (colour + " " + thing) for colour in colours for thing in things ]
print(coloured_things)

# Dict Comprehension
# result = {'key':f(x) for list[x] if condition}
squares = {str(x):x**2 for x in range(10) if (x % 2 == 0)}
print(squares)

# Lambda
#inline function definition
# create a function with the name inlineFunc
inlineFunc = lambda x: x + 1
# call the new function
print(inlineFunc(1)) # returns 1 + 1 = 2


# Map with Lambda
result = list(map(lambda x: x+1,[1,2,3,4,5])) # returns: [2,3,4,5,6]
print(result)

# Sets
# define two sets
a = {1,2,3,4,5}
b = {4,5,6,7,8}
d = {1,2,3}
# OR (all elements of a and b)
print(a | b) # {1, 2, 3, 4, 5, 6, 7, 8}
# AND (all elements that are in both)
print(a & b) # {4, 5}
# symmetric Difference XOR (all that are only in one of both)
print(a ^ b) #{1, 2, 3, 6, 7, 8}
# Difference (remove all items from a that are in b too)
print(a - b) # {1,2,3}
# is (sub-) set of
print(a <= b) # False
print(d <= a) # True


A = {('I2', 'I3'), ('I4', 'I3'), ('I2', 'I4'), ('I2', 'I5'), ('I5', 'I3'), ('I5', 'I4'), ('I1', 'I2'), ('I1', 'I3'), ('I1', 'I4'), ('I1', 'I5')}
B = {'T100': ('I1', 'I2', 'I5'), 'T200': ('I2', 'I4'), 'T300': ('I2', 'I3'), 'T400': ('I1', 'I2', 'I4'), 'T500': ('I1', 'I3'), 'T600': ('I2', 'I3'), 'T700': ('I1', 'I3'), 'T800': ('I1', 'I2', 'I3', 'I5'), 'T900': ('I1', 'I2', 'I3')}

def calc_supports(itemsets, old_itemsets):
    supports = {}
    for itemset in itemsets:
        print("#############################", itemset)
        if itemset not in supports:
            supports[itemset] = 0
        for ois in old_itemsets:
            print(old_itemsets[ois], set(itemset).issubset(old_itemsets[ois]))
            if set(itemset).issubset(old_itemsets[ois]):
                supports[itemset] += 1
    return supports


print(calc_supports(A,B))