import requests, datetime
from bs4 import BeautifulSoup

COBURG_URL = 'https://www.coburg.de/desktopdefault.aspx/tabid-2754/2848_read-16990/'

now = datetime.datetime.now()
curr_minute = now.strftime("%d.%m.%Y %H:%M")


def get_latest_value():
    with requests.Session() as s:
        soupurl = requests.get(COBURG_URL).text
        soup = BeautifulSoup(soupurl, 'html.parser')
        main = soup.find('div', {'id':'ctl01_contentpane'}) 
        paragraphs = soup.find_all("p")
        value_string = paragraphs[2].contents[4].contents[2].contents[0][-5:]
        value = float(value_string.replace('.','').replace(',','.'))
        return value

curr_value = get_latest_value()
print("Indizewert für Coburg für:", curr_minute, ":", curr_value)