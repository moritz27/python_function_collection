import datetime as dt

now = dt.datetime.now()
print(now)

def to_millis_timestamp(input):
    if isinstance(input, dt.datetime):
        mydatetime = input
    elif isinstance(input, str):
        mydatetime = dt.datetime.strptime(input, "%d.%m.%Y %H:%M:%S")
    return (int(dt.datetime.timestamp(mydatetime)*1000))

print(to_millis_timestamp(now))

def to_seconds_timestamp(input):
    if isinstance(input, dt.datetime):
        mydatetime = input
    elif isinstance(input, str):
        mydatetime = dt.datetime.strptime(input, "%d.%m.%Y %H:%M:%S")
    return (int(dt.datetime.timestamp(mydatetime)))

test_ts = to_seconds_timestamp(now)
print(test_ts)

def convert_to_readable_times(timestamp):
    return str(dt.datetime.fromtimestamp(timestamp))

print(convert_to_readable_times(test_ts))